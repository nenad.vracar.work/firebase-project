const firebase = require('../server/firebase');

class Service {
    constructor(name) {
        this.name = name;
        this.database = firebase.firestore();
        this.collection = this.database.collection(this.name);
    }

    toArray(snapshot) {
        return snapshot.docs.map(doc => ({ ...doc.data(), id: doc.id }));
    }

}

module.exports = Service;