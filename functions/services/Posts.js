const Service = require('./Service');

class Posts extends Service {
    constructor(props) {
        super(props);
        this.getAll = this.getAll.bind(this);
    }

    async getAll() {
        const snapshot = await this.collection.get();
        return this.toArray(snapshot);
    }

    async getOne(docId) {
        const snapshot = await this.collection.doc(docId).get();
        const data = snapshot.data();
        const id = snapshot.id;
        return { ...data, id }
    }

    async insertPost({ title, body }) {
        const post = { title, body }
        const response = await this.collection.doc().set(post);
        return response;
    }

    async updatePost(docId, update) {
        const response = await this.collection.doc(docId).update(update);
        return response;
    }

    async deletePost(docId) {
        const response = this.collection.doc(docId).delete();
        return response; 
    }
}

module.exports = Posts;