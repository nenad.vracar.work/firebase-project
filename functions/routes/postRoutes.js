const express = require('express');
const router = express.Router();
const PostController = require('../controllers/PostController');

const postController = new PostController();

router.get('/', postController.index);
router.get('/:id', postController.show);
router.post('/', postController.create);
router.delete('/:id', postController.delete);
router.patch('/:id', postController.update);

module.exports = router;