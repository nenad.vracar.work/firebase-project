const PostService = require('../services/Posts');

class PostController {
    constructor() {
        this.service = new PostService('posts');
        this.index = this.index.bind(this);
        this.create = this.create.bind(this);
        this.delete = this.delete.bind(this);
        this.update = this.update.bind(this);
        this.show = this.show.bind(this);
    }

    async index(req, res) {
        const response = await this.service.getAll();
        res.send(response);
    }

    async create(req, res) {
        const response = await this.service.insertPost(req.body);
        res.send(response);
    }

    async delete(req, res) {
        const id = req.params.id;
        const response = await this.service.deletePost(id);
        res.send(response);
    }

    async show(req, res) {
        const id = req.params.id;
        const response = await this.service.getOne(id);
        res.send(response);
    }

    async update(req, res) {
        const id = req.params.id;
        const update = req.body;
        const response = await this.service.updatePost(id, update);
        res.send(response);
    }
}

module.exports = PostController;