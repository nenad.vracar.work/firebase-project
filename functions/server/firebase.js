const admin = require('firebase-admin');
const credentials = require('../config/firebase.json');

const firebase = admin.initializeApp({
    credential: admin.credential.cert(credentials),
    databaseURL: 'https://test-app-b69d7.firebaseio.com/'
})

module.exports = firebase;