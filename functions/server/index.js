const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const postRoutes = require('../routes/postRoutes');

const server = express();

server.use(cors());
server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())

server.use('/posts', postRoutes);

module.exports = server;
